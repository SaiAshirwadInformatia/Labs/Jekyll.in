---
layout: post
title: "Jekyll Retrospect"
source_url: "https://gitlab.com/cpatil/Jekyll-Retrospect"
demo_url: "https://cpatil.gitlab.io/Jekyll-Retrospect"
license_url: "https://gitlab.com/cpatil/Jekyll-Retrospect/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-retrospect"
category: Templates
author: cpatil
---
 Just a cool, modern landing page to showcase whatever it is you're about.