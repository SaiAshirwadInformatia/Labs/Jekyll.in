---
layout: post
title: "Jekyll Introspect"
source_url: "https://gitlab.com/cpatil/Jekyll-Introspect"
demo_url: "https://cpatil.gitlab.io/Jekyll-Introspect"
license_url: "https://gitlab.com/cpatil/Jekyll-Introspect/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-introspect"
category: Templates
author: cpatil
---
This is Introspect, an ultra-minimal landing page template with a big cover photo.