---
layout: post
title: "Jekyll Fullmotion"
source_url: "https://gitlab.com/cpatil/Jekyll-Fullmotion"
demo_url: "https://cpatil.gitlab.io/Jekyll-Fullmotion"
license_url: "https://gitlab.com/cpatil/Jekyll-Fullmotion/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-fullmotion"
category: Templates
author: cpatil
---
A full responsive video gallery template with a functional video lightbox.