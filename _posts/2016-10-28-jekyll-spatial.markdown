---
layout: post
title: "Jekyll Spatial"
source_url: "https://gitlab.com/cpatil/Jekyll-Spatial"
demo_url: "https://cpatil.gitlab.io/Jekyll-Spatial"
license_url: "https://gitlab.com/cpatil/Jekyll-Spatial/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-spatial"
category: Templates
author: cpatil
---
A simple, spacious, minimalistic design topped off with a large cover image.