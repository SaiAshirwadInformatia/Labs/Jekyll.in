---
layout: post
title:  "Jekyll Visualize"
source_url: "https://gitlab.com/cpatil/Jekyll-Visualize"
demo_url: "https://cpatil.gitlab.io/Jekyll-Visualize"
license_url: "https://gitlab.com/cpatil/Jekyll-Visualize/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-visualize"
category: Templates
author: cpatil
---
Visualize is simple, single page portfolio design with a fully functional lightbox.
