---
layout: post
title: "Jekyll Transit"
source_url: "https://gitlab.com/cpatil/Jekyll-Transit"
demo_url: "https://cpatil.gitlab.io/Jekyll-Transit"
license_url: "https://gitlab.com/cpatil/Jekyll-Transit/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-transit"
category: Templates
author: cpatil
---
A clean, minimal design that gets straight to the point.Built with app devs/startups in mind.