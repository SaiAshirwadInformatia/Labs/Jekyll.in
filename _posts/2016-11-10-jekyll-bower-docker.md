---
layout: post
title: Jekyll Bower Docker
source_url: "https://gitlab.com/SaiLabs/JekyllBowerDocker"
license_url: "https://gitlab.com/SaiLabs/JekyllBowerDocker/blob/master/LICENSE"
license: "MIT"
category: Dockers
docker: saiashirwadinformatia/jekyll-bower
author: rsakhale
---
Build your jekyll site over CI easily using this pre-built Jekyll Bower Docker