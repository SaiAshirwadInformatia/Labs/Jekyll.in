---
layout: post
title: "Jekyll Interphase"
source_url: "https://gitlab.com/cpatil/Jekyll-Interphase"
demo_url: "https://cpatil.gitlab.io/Jekyll-Interphase"
license_url: "https://gitlab.com/cpatil/Jekyll-Interphase/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-interphase"
category: Templates
author: cpatil
---
Just a super simple responsive design that'll work for just about anything.