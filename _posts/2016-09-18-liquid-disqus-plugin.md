---
layout: post
title: Liquid Disqus Plugin
source_url: "https://gitlab.com/SaiLabs/Liquid-Disqus"
license_url: "https://gitlab.com/SaiLabs/Liquid-Disqus/blob/master/LICENSE.md"
license: "MIT"
gem: "liquid-disqus"
category: Plugins
author: rsakhale
---
Add Disqus comment system to your Jekyll based sites easily