---
layout: post
title: Jekyll Author Pages Plugin
source_url: "https://gitlab.com/SaiLabs/Jekyll-Author-Pages"
license_url: "https://gitlab.com/SaiLabs/Jekyll-Author-Pages/blob/master/LICENSE.md"
license: "MIT"
gem: "jekyll-author-pages"
category: Plugins
author: rsakhale
---
Generate pages for your post author and let them shine for their contribution.
