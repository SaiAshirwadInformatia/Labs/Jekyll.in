---
layout: post
title: "Jekyll Grunt Plugin"
source_url: "https://gitlab.com/SaiLabs/Jekyll-Grunt"
license_url: "https://gitlab.com/SaiLabs/Jekyll-Grunt/blob/master/LICENSE.md"
license: "MIT"
gem: "jekyll-grunt"
category: Plugins
author: rsakhale
---
Run your Grunt tasks while developing your static site with Jekyll-Grunt plugin