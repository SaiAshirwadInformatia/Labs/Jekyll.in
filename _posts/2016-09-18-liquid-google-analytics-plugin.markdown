---
layout: post
title: "Liquid Google Analytics Plugin"
source_url: "https://gitlab.com/SaiLabs/LiquidGoogleAnalytics"
license_url: "https://gitlab.com/SaiLabs/LiquidGoogleAnalytics/blob/master/LICENSE.md"
license: "MIT"
gem: "liquid-google-analytics"
category: Plugins
author: rsakhale
---
Generate your google analytics code with simple Liquid Tag