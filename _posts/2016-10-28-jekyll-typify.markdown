---
layout: post
title:  "Jekyll Typify"
source_url: "https://gitlab.com/cpatil/Jekyll-Typify"
demo_url: "https://cpatil.gitlab.io/Jekyll-Typify"
license_url: "https://gitlab.com/cpatil/Jekyll-Typify/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-typify"
category: Templates
author: cpatil
---
A simple one pager ideal for pretty much anything.