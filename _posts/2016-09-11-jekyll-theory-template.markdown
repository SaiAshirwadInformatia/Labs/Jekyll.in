---
layout: post
title:  "Jekyll Theory"
source_url: "https://gitlab.com/rsakhale/Jekyll-Theory"
demo_url: "https://rsakhale.gitlab.io/Jekyll-Theory/demo/"
license_url: "https://gitlab.com/rsakhale/Jekyll-Theory/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-theory"
category: Templates
author: rsakhale
---
Just a crisp, modern landing page template ideal for businesses and corporations.