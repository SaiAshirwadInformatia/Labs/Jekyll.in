---
layout: post
title: "Jekyll Prism"
source_url: "https://gitlab.com/cpatil/Jekyll-Prism"
demo_url: "https://cpatil.gitlab.io/Jekyll-Prism"
license_url: "https://gitlab.com/cpatil/Jekyll-Prism/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-prism"
category: Templates
author: cpatil
---
 Welcome to Prism, a free responsive site template.