---
layout: post
title: "Jekyll Epilogue"
source_url: "https://gitlab.com/cpatil/Jekyll-Epilogue"
demo_url: "https://cpatil.gitlab.io/Jekyll-Epilogue"
license_url: "https://gitlab.com/cpatil/Jekyll-Epilogue/blob/master/LICENSE.txt"
license: "MIT"
gem: "jekyll-epilogue"
category: Templates
author: cpatil
---
A slick landing page with clean lines and a minimalistic look and feel.