---
layout: post
title: "Jekyll Bower Plugin"
source_url: "https://gitlab.com/SaiLabs/Jekyll-Bower"
license_url: "https://gitlab.com/SaiLabs/Jekyll-Bower/blob/master/LICENSE.md"
license: "MIT"
gem: "jekyll-bower"
category: Plugins
author: rsakhale
---
Install your bower dependencies easily with Jekyll-Bower