---
layout: page
title: Templates
permalink: /templates/
---
{% for post in site.posts %}
{% if post.category == "Templates" %}
<div class="row panel-gem" data-gem="{{ post.gem }}" style="padding-top: 15px;padding-bottom: 15px;">
	<div class="col-sm-3 col-xs-4">
		{% if post.thumbnail %}
		<img src="{{ site.baseurl }}{{ post.thumbnail }}" alt="Thumbnail: {{ post.title }}" style="width: 100%" class="img-thumbnail img-responsive" />
		{% endif %}
		<div class="btn-group visible-xs">
			{% if post.category contains "Templates" and post.demo_link %}
			<a class="btn btn-default btn-xs" href="{{ site.baseurl }}/{{ post.demo_link }}">
				Preview
			</a>
			{% endif %}
			<a class="btn btn-default btn-xs" href="{{ site.baseurl }}{{ post.url }}">
				Read More
			</a>
		</div>
	</div>
	<div class="col-sm-9 col-xs-8">
		<h3>
			<a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a>
		</h3>
		<p>
		{{ post.content }}
		</p>
		<div class="author-block">
			<span>Contributed by</span> 
			<i class="fa fa-user"></i>
			<span class="gem-authors"><a href="{{ site.baseurl }}{{ post.author.url }}">{{ post.author.name }}</a></span>
		</div>
		<div class="row" style="margin-right: 0;margin-left: 0;">
			<div class="col-xs-4 text-center">
				<span>Version</span>
				<i class="fa fa-code-fork"></i>
				<span class="gem-version">0</span>
			</div>
			<div class="col-xs-4 text-center">
				<span>License</span>
				<i class="fa fa-cubes"></i>
				<span class="gem-license">MIT</span>
			</div>
			<div class="col-xs-4 text-center">
				<span>Downloads</span>
				<i class="fa fa-cloud-download"></i>
				<span class="gem-downloads">0</span>
			</div>
		</div>
		<div class="btn-group hidden-xs">
			{% if post.category contains "Templates" and post.demo_link %}
			<a class="btn btn-default" href="{{ site.baseurl }}/{{ post.demo_link }}">
				Preview
			</a>
			{% endif %}
			<a class="btn btn-default" href="{{ site.baseurl }}{{ post.url }}">
				Read More
			</a>
		</div>
	</div>
</div>
<hr />
{% endif %}
{% endfor %}
<script>
$(function(){
	$('.panel-gem').each(function(){
		var gem = $(this).data('gem');
		var instance = this;
		$.ajax({
			async: true,
			cache: false,
			url: 'https://rubygems.org/api/v1/gems/' + gem + '.json',
			success: function(response){
				$(instance).find('.gem-version').html(response.version);
				$(instance).find('.gem-downloads').html(response.downloads);
				if(response.licenses !== undefined){
					$(instance).find('.gem-license').html(response.licenses.join(","));
				}
			},
			error: function(response){

			}
		});
	});
});
</script>
{% include google_leader_ad.html %}