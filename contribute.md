---
layout: page
title: Contribute
permalink: /contribute/
---
<h3>Steps</h3>

<ol>
	<li>Fork this <a href="{{ site.gitlab_url }}">project</a></li>
	<li>Create your branch for updates</li>
	<li>Add your changes</li>
	<li>Create a new merge request</li>
</ol>
