---
layout: page
title: Dockers
permalink: /dockers/
---
{% for post in site.posts %}
{% if post.category == "Dockers" %}
<div class="row panel-gem" style="padding-top: 15px;padding-bottom: 15px;">
	<div class="col-sm-12">
		<h3>
			<a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a>
		</h3>
		<p>
		{{ post.content }}
		</p>
		<div class="author-block">
			<span>Contributed by</span> 
			<i class="fa fa-user"></i>
			<span class="gem-authors"><a href="{{ site.baseurl }}{{ post.author.url }}">{{ post.author.name }}</a></span>
		</div>
		<div class="btn-group visible-xs">
			<a class="btn btn-default btn-xs" href="{{ site.baseurl }}{{ post.url }}">
				Read More
			</a>
		</div>
		<div class="btn-group hidden-xs">
			<a class="btn btn-default" href="{{ site.baseurl }}{{ post.url }}">
				Read More
			</a>
		</div>
	</div>
</div>
<hr />
{% endif %}
{% endfor %}
<script>
$(function(){
	
});
</script>
{% include google_leader_ad.html %}