# Fetch Source Info
require 'rest-client'
module SaiJekyll
	class SourceInfo
		def initialize(site)
			puts "Fetching README information"
			site.posts.docs.each do |post|
				if post['source_url']
					post.data['source_readme'] = fetchReadme(post['source_url'])
				end
			end
		end

		def fetchReadme(source_url)
			if source_url.include? 'https://gitlab.com'
				puts "Fetching GitLab Project Readme: " + source_url + "\n"
				source_url = source_url + '/raw/master/README.md'
			elsif source_url.include? 'https://github.com'
				puts "Fetching GitHub Project Readme: " + source_url + "\n"
				source_url = source_url + '/master/README.md'
			end
			begin
				RestClient.get source_url 
			rescue Exception => e
				puts "Error Occured on fetching README (" + source_url + "): " + e.message
			end
		end
	end
end

Jekyll::Hooks.register :site, :post_read do |jekyll| # jekyll here acts as site global object
	SaiJekyll::SourceInfo.new(jekyll)
end