module TemplateDemo

	class DemoPage < Jekyll::Page
		def initialize(site, base, dir, post)
			@site = site
			@base = base
			@dir = dir
			@name = 'index.html'

			puts "Directory: " + dir
			self.process(@name)
			self.read_yaml(File.join(base, '_layouts'), 'demo.html')
			puts "Generated Demo Page successfully: " + post.data['title']

			post.data['demo_link'] = dir
			post.data['previous'] = post.previous_doc
			post.data['next'] = post.next_doc
			self.data['title'] = post.data['title']
			self.data['post'] = post
			
		end
	end

	class Generator < Jekyll::Generator
		safe true

		def generate(site)
			puts "Generating Demo Pages\n"	
			dir = site.config['demo_dir'] || 'demo'
			puts "Demo pages directory: " + dir  + "\n"
			site.posts.docs.each_with_index do |post, post_index|
				site.pages << DemoPage.new(site, site.source, File.join(dir, post.data['slug']), post)				
			end			
		end
	end
end