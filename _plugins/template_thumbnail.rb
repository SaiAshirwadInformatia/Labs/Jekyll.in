# Generate thumbnail on runtime
require 'fileutils'
require 'digest/md5'
module SaiJekyll
	class GenerateThumbnailHash
		def initialize(site)
			puts "Generating Thumbnails Hash for templates"
			site.posts.docs.each do |post|
				if post['demo_url']
					post.data['thumbnail_hash'] = generateHash(post['demo_url'])
					post.data['thumbnail'] = '/images/' + post.data['thumbnail_hash']
					post.data['cover_hash'] = generateHash(post['demo_url'] + '_cover')
					post.data['cover'] = '/images/' + post.data['cover_hash']
				end
			end
		end
		def generateHash(url)
			Digest::MD5.hexdigest(url) + '.png'
		end
	end
	class GenerateThumbnail
		def initialize(site)
			puts "Generating Thumbnails for templates"
			images_dir = File.join(site.config['destination'], 'images')
			unless File.directory?(images_dir)
				FileUtils.mkdir_p(images_dir)
			end
			@thumbnail_dir = images_dir
			site.posts.docs.each do |post|
				if post['demo_url']
					unless File.exist?(File.join(@thumbnail_dir, post['thumbnail_hash']))
						post.data['thumbnail'] = generateThumbnail(post['demo_url'], post['thumbnail_hash'])
					else
						puts "#{post['thumbnail_hash']} exists skipping thumbnail generation" + "\n"
					end
					unless File.exist?(File.join(@thumbnail_dir, post['cover_hash']))
						post.data['cover'] = generateCover(post['demo_url'], post['cover_hash'])
					else
						puts "#{post['cover_hash']} exists skipping cover generation" + "\n"
					end
				end
			end
		end

		def generateCover(url, filename)
			cover_path = File.join(@thumbnail_dir, filename)
			puts "Generating cover " + url + "\n"
			begin
				system `wkhtmltoimage --height 600 --width 1920 --quality 60 #{url} #{cover_path}`
			rescue Exception => e
				puts "Error Occured on Cover Generation (" + url + "): " + e.message
			end
			imagepath = '/images/' + filename
			imagepath
		end

		def generateThumbnail(url, filename)
			thumbnail_path = File.join(@thumbnail_dir, filename)
			puts "Generating thumbnail " + url + "\n"
			begin
				system `wkhtmltoimage --height 350 --width 200 --quality 60  #{url} #{thumbnail_path}`
			rescue Exception => e
				puts "Error Occured on Thumbnail Generation (" + url + "): " + e.message
			end
			imagepath = '/images/' + filename
			imagepath
		end
	end
end

Jekyll::Hooks.register :site, :post_read do |jekyll| # jekyll here acts as site global object
	SaiJekyll::GenerateThumbnailHash.new(jekyll)
end

Jekyll::Hooks.register :site, :post_write do |jekyll| # jekyll here acts as site global object
	SaiJekyll::GenerateThumbnail.new(jekyll)
end