# Fetch GEM Info and append to every post
require 'rest-client'
module SaiJekyll
	class GemInfo
		def initialize(site)
			puts "Fetching Gem information"
			site.posts.docs.each do |post|
				if post['gem']
					puts "Fetching info for gem: " + post['gem']
					post.data['gem_info'] = fetchGem(post['gem'])
				end
			end
		end

		def fetchGem(gem)
			begin
				JSON.parse(RestClient.get 'https://rubygems.org/api/v1/gems/' + gem + '.json')
			rescue Exception => e
				puts "Error Occured on GEM fetching (" + gem + "): " + e.message
			end
		end
	end
end

Jekyll::Hooks.register :site, :post_read do |jekyll| # jekyll here acts as site global object
	SaiJekyll::GemInfo.new(jekyll)
end